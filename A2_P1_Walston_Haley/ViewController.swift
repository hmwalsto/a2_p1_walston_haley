//
//  ViewController.swift
//  A2_P1_Walston_Haley
//
//  Created by Haley Walston on 9/15/19.
//  Copyright © 2019 Haley Walston. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var displayCalc: UILabel!
    var calculating = false
    var num2: Double = 0
    var num1: Double = 0
    var result: Double = 0
    var operation = 0
    var isEmpty = true
    var decimalUsed = false
    var resetNow = false
    
    func resetAll(){
        calculating = false
        num1 = 0
        num2 = 0
        result = 0
        operation = 0
        isEmpty = true
        decimalUsed = false
        resetNow = false
        displayCalc.backgroundColor = UIColor.white
        displayCalc.text = "0"
        displayCalc.textColor = UIColor.black
    }
    
    func errorMade(){
        resetNow = true
        displayCalc.text = "0"
        displayCalc.backgroundColor = UIColor.red
    }
    
    @IBAction func numbers(_ sender: UIButton) {
        let currentNum = sender.tag - 1
        let buttonPushed: Int = (sender as AnyObject).tag
        
        if resetNow == true{ resetAll() }
        
        //decimal button
        if buttonPushed == 15{
            if decimalUsed == true { //two decimals back to back error
                errorMade()
            } else if decimalUsed == false{
                decimalUsed = true
                displayCalc.text! += "."
            }
        }
        
        //second number
        if calculating == true{
            if buttonPushed != 15{
                displayCalc.text! += String(currentNum)
                
                //store num2 from full label string
                var temp = String(displayCalc.text!)
                let seperators = CharacterSet(charactersIn: "+,-,*,/")
                let tempArray = temp.components(separatedBy: seperators)
                temp = tempArray[1]
                num2 = (temp as NSString).doubleValue
            }
            
            //first number
        } else if calculating == false && buttonPushed != 15 { 
            displayCalc.backgroundColor = UIColor.white
            if decimalUsed == true{
                displayCalc.text! += String(currentNum)
                num1 = Double(displayCalc.text!)!
                isEmpty = false
            } else if decimalUsed == false{
                if displayCalc.text == "0" {
                    displayCalc.text = ""
                }
                displayCalc.text! += String(currentNum)
                num1 = Double(displayCalc.text!)!
                isEmpty = false
            }
        }
    }
    
    @IBAction func clearAll(_ sender: Any) {
            resetAll()
    }
    
    @IBAction func performCalculation(_ sender: Any) {
        let buttonPushed: Int = (sender as AnyObject).tag
        
        if buttonPushed < 15 && isEmpty == false && calculating == false {
            switch(buttonPushed){
            case 11:
                displayCalc.text! += "*"
                break
            case 12:
                displayCalc.text! += "/"
                break
            case 13:
                displayCalc.text! += "-"
                break
            case 14:
                displayCalc.text! += "+"
                break
            default:
                break
            }
            calculating = true
            operation = (sender as AnyObject).tag
            decimalUsed = false //reset for next number
        } else if buttonPushed == 16 { //equal button
            resetNow = true
        
            switch(operation){
                case 11:
                    displayCalc.text! = String(num1 * num2)
                    break
                case 12:
                    displayCalc.text! = String(num1 / num2)
                    break
                case 13:
                    displayCalc.text! = String(num1 - num2)
                    break
                case 14:
                    displayCalc.text! = String(num1 + num2)
                    break
                default:
                    break
            }
            
            result = (displayCalc.text! as NSString).doubleValue
            
            if result < 0 {
                displayCalc.backgroundColor = UIColor.black
                displayCalc.textColor = UIColor.white
            } else if result >= 0 {
                displayCalc.backgroundColor = UIColor.lightGray
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
}

